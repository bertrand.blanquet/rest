package com.quad;

import com.quad.api.RestApplication;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.Assert;

@SpringBootTest(classes = RestApplication.class)
class RestApplicationTests {

	@Test
	void DoSomething() {
		Assert.isTrue(true,"it was supposed to be true");
	}

	@Test
	void DoSomethingElse() {
		Assert.isTrue(true,"it was supposed to be true");
	}


}
