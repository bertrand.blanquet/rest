package com.quad.core;

import java.awt.*;

public class CanvasItem implements Drawable {
    public Color FillColor = Color.CYAN;
    public Color StrokeColor = Color.black;
    public String Label = "Text";
    public int StrokeThickness = 0;
    public BoundingBox Box;

    public CanvasItem(int x, int y, int width, int height){
        this.Box = new BoundingBox(x,y,width,height);
    }

    @Override
    public BoundingBox getBox() {
        return this.Box;
    }

    @Override
    public void paint(Graphics2D g) {
        g.setPaint(this.FillColor);
        g.fillRect(this.Box.getX(),this.Box.getY(),this.Box.getWidth(),this.Box.getHeight());
        g.setPaint(this.StrokeColor);
        var itemStroke =
                g.getStroke()
                        .createStrokedShape(
                                new Rectangle(this.Box.getX(),this.Box.getY(),this.Box.getWidth(),this.Box.getHeight()));
        g.setPaint(Color.white);
        g.drawString(this.Label,this.Box.right(),this.Box.bottom());
        g.draw(itemStroke);
    }

    @Override
    public String getLabel() {
        return this.Label;
    }
}
