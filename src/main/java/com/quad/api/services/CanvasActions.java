package com.quad.api.services;

import com.quad.api.model.Canvas;
import com.quad.api.model.ResearchInfo;
import com.quad.core.BoundingBox;

import java.io.IOException;
import java.util.List;

public interface CanvasActions {
    List<Canvas> list();
    byte[] getImg(String name) throws IOException;
    void add(String name, int width, int height, int threshold);
    List<ResearchInfo> search(String canvas, BoundingBox box);
    boolean isFull();
    boolean exist(String name);
}
