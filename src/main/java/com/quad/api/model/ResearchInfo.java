package com.quad.api.model;

import java.util.List;

public class ResearchInfo {
    public String structure;
    public List<String> foundItems;
    public String duration;

    public ResearchInfo(String label, List<String> foundItems, String duration) {
        structure =label;
        this.foundItems = foundItems;
        this.duration = duration;
    }
}
