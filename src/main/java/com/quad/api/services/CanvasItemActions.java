package com.quad.api.services;

import com.quad.api.model.Item;
import com.quad.core.BoundingBox;

import java.util.List;
import java.util.stream.Stream;

public interface CanvasItemActions {
    boolean random(String label, int count);
    boolean add(String name, String canvas, Stream<BoundingBox> box);
    List<Item> list();
    boolean isFull();
}
