package com.quad.api.controllers;

import com.quad.api.model.Canvas;

import com.quad.api.model.ResearchInfo;
import com.quad.api.services.CanvasActions;
import com.quad.core.BoundingBox;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.*;
import java.util.List;


@RestController
@RequestMapping("/api/quad")
public class CanvasController {
    @Autowired
    public CanvasActions Service;

    @PostMapping(value = "/canvas")
    @ResponseBody
    public ResponseEntity add(String name, int width, int height, int threshold){
        if(this.Service.exist(name)){
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE)
                    .contentType(MediaType.TEXT_HTML)
                    .body("Sorry, the name is already used.");
        }

        if(this.Service.isFull()){
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE)
                    .contentType(MediaType.TEXT_HTML)
                    .body("Sorry, the database is already full.");
        }else{
            this.Service.add(name,width,height,threshold);
            return new ResponseEntity(HttpStatus.OK);
        }
    }

    @GetMapping(value = "/canvas")
    @ResponseBody
    public ResponseEntity<List<Canvas>> list(){
        return ResponseEntity.status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .body(this.Service.list());
    }

    @RequestMapping(value = "/canvas/{name}/items/", method = RequestMethod.GET)
    public ResponseEntity<List<ResearchInfo>> search(@PathVariable(value="name") String canvas, BoundingBox box){
        return ResponseEntity.status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .body(this.Service.search(canvas,box));
    }

    @RequestMapping(value = "/canvas/{name}", method = RequestMethod.GET)
    public ResponseEntity<Object> img(@PathVariable(value="name") String name) throws IOException {
        var img = Service.getImg(name);
        if(img != null){
            return ResponseEntity.status(HttpStatus.OK)
                    .contentType(MediaType.IMAGE_JPEG)
                    .body(img);
        }else{
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE)
                    .contentType(MediaType.TEXT_HTML)
                    .body("Could not generate jpg, Are you sure the canvas is defined?");
        }
    }
}