package com.quad.api.model;

import com.quad.core.BoundingBox;
import com.quad.core.Boxable;

import javax.persistence.*;
import java.awt.*;

@Entity
@Table(name = "TITEM")
public class Item implements Boxable {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private int id;
    @Column(name = "CanvasId")
    private int CanvasId;
    @Column(name = "Label")
    private String Label;
    @Column(name = "X")
    private int x;
    @Column(name = "Y")
    private int y;

    @Column(name = "Width")
    private int width;
    @Column(name = "Height")
    private int height;
    @Column(name = "fillcolor")
    private String fillColor;
    @Column(name = "strokecolor")
    private String strokeColor;

    public void SetFillColor(Color color){
        this.fillColor = Integer.toString(color.getRGB());
    }

    public Color GetFillColor(){
        var color = Integer.parseInt(this.fillColor);
        return new Color(color);
    }

    public void SetStrokeColor(Color color){
        this.strokeColor = Integer.toString(color.getRGB());
    }

    public Color GetStokeColor(){
        var color = Integer.parseInt(this.strokeColor);
        return new Color(color);
    }

    public Item(int canvasId, String label, int x, int y, int width, int height) {
        this.CanvasId = canvasId;
        Label = label;
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.SetFillColor(Color.RED);
        this.SetStrokeColor(Color.blue);
    }

    public Item(){

    }

    @Override
    public BoundingBox getBox() {
        return new BoundingBox(this.x,this.y,this.width,this.height);
    }

    @Override
    public String getLabel() {
        return this.Label;
    }

    public int getId() {
        return id;
    }

    public int getCanvasId() {
        return CanvasId;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public String getFillColor() {
        return fillColor;
    }

    public String getStrokeColor() {
        return strokeColor;
    }
}
