package com.quad.core;

public interface Labeled {
    String getLabel();
}
