package com.quad.api.services;

import com.quad.api.model.Item;
import com.quad.api.repositories.CanvasItemRepository;
import com.quad.core.BoundingBox;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

@Service
public class CanvasItemService implements CanvasItemActions {
    @Autowired
    public CanvasItemRepository Repository;
    @Autowired
    public com.quad.api.repositories.CanvasRepository CanvasRepository;
    public boolean add(String name, String canvas, Stream<BoundingBox> boxes) {
        var candidate = this.CanvasRepository.getCanvasFromName(canvas);
        if(candidate.isPresent()){
            var box = boxes.map(b->
                    new Item(candidate.get().getId(),name,b.getX(),b.getY(),b.getWidth(),b.getHeight())
            );
            this.Repository.saveAll(box.toList());
            return true;
        }else{
            return false;
        }
    }

    public boolean random(String label, int count){
        var maybeCanvas = this.CanvasRepository.getCanvasFromName(label);
        if(maybeCanvas.isPresent()){
            var canvas = maybeCanvas.get();
            var newItems = new ArrayList<Item>();
            for (var i = 0; i < count; i++){
                var x = (int) Math.round(Math.random()*(canvas.getWidth() -15));
                var y = (int) Math.round(Math.random()*(canvas.getHeight() -15));
                newItems.add(new Item(canvas.getId(),"R"+i,x,y,10,10));
            }
            this.Repository.saveAll(newItems);
            return true;
        }else{
            return false;
        }
    }

    public List<Item> list() {
        return this.Repository.findAll();
    }

    @Override
    public boolean isFull() {
        return 10000 < this.CanvasRepository.count();
    }
}
