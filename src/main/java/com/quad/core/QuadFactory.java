package com.quad.core;

import com.quad.core.collection.QuadTree;

import java.util.ArrayList;
import java.util.List;

public class QuadFactory {
    public static QuadTree get(){
        var tree = new QuadTree(new BoundingBox(0,0,1000,1000),5);
        QuadFactory.getSix().forEach(bb->{
            tree.add(bb);
        });
        return tree;
    }
    private static ArrayList<Drawable> getSix(){
        return new ArrayList<Drawable>(List.of(
                new CanvasItem(1,1,10,10),
                new CanvasItem(10,1,10,10),
                new CanvasItem(1,10,10,10),
                new CanvasItem(30,30,10,10),
                new CanvasItem(30,35,10,10),
                new CanvasItem(30,40,10,10),
                new CanvasItem(35,30,10,10),
                new CanvasItem(20,30,10,10),
                new CanvasItem(30,60,10,10),
                new CanvasItem(80,30,10,10),
                new CanvasItem(30,100,10,10)));
    }
}