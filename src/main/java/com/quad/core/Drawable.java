package com.quad.core;

import java.awt.*;

public interface Drawable extends Boxable {
    void paint(Graphics2D g);
}
