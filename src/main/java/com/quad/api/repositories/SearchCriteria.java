package com.quad.api.repositories;

public class SearchCriteria<T> {
    private String key;
    private String operation;
    private Object value;

    public SearchCriteria(String k, String operation, Object value) {
        this.key = k;
        this.operation = operation;
        this.value = value;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }
}