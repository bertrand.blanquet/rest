package com.quad.api.model;

import javax.persistence.*;

@Entity
@Table(name = "TCANVAS")
public class Canvas {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private int id;
    @Column(name = "label")
    private String Label;
    @Column(name = "width")
    private int width;
    @Column(name = "height")
    private int height;
    @Column(name = "threshold")
    private int threshold;

    public Canvas(){}

    public Canvas(String label, int width, int height, int threshold) {
        this.Label = label;
        this.width = width;
        this.height = height;
        this.threshold = threshold;
    }

    public int getId() {
        return id;
    }

    public String getLabel() {
        return Label;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public int getThreshold() {
        return threshold;
    }
}
