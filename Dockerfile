FROM openjdk:latest
COPY build/libs/rest-0.0.1-SNAPSHOT.jar rest-0.0.1-SNAPSHOT.jar
EXPOSE 8090
CMD java -jar rest-0.0.1-SNAPSHOT.jar