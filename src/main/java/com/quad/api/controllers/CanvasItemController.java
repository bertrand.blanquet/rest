package com.quad.api.controllers;

import com.quad.api.services.CanvasItemActions;
import com.quad.api.services.CanvasService;
import com.quad.core.BoundingBox;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;

@RestController
@RequestMapping("/api/quad")
public class CanvasItemController {
    @Autowired
    public CanvasItemActions Service;

    @Autowired
    public CanvasService CanvasService;

    @PostMapping(value = "/item")
    @ResponseBody
    public ResponseEntity add(String name, String canvas, BoundingBox box){
        if(this.Service.isFull()){
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE)
                    .contentType(MediaType.TEXT_HTML)
                    .body("Sorry, the database is already full.");
        }

        if(!CanvasService.exist(canvas)) {
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE)
                    .contentType(MediaType.TEXT_HTML)
                    .body("Sorry, the canvas doesn't exist.");
        }

        var result = this.Service.add(name,canvas, Arrays.asList(box).stream());
        if(result){
            return new ResponseEntity(HttpStatus.OK);
        }else{
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE)
                    .contentType(MediaType.TEXT_HTML)
                    .body("Could not add item.");
        }
    }

    @RequestMapping(value = "/canvas/{name}/items/random", method = RequestMethod.POST)
    public ResponseEntity random(@PathVariable(value="name") String name, int count){
        if(this.Service.isFull()){
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE)
                    .contentType(MediaType.TEXT_HTML)
                    .body("Sorry, the database is already full.");
        }

        if(!CanvasService.exist(name)) {
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE)
                    .contentType(MediaType.TEXT_HTML)
                    .body("Sorry, the canvas doesn't exist.");
        }

        if(count < 51 ){
            try{
                this.Service.random(name,count);
                return new ResponseEntity(HttpStatus.OK);
            }catch (Exception e){
                return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE)
                        .contentType(MediaType.TEXT_HTML)
                        .body("Could not add item. " + e.getMessage()) ;
            }
        }else{
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE)
                    .contentType(MediaType.TEXT_HTML)
                    .body("Sorry, cannot add more than 50 items a time.");
        }
    }

    @GetMapping(value = "/items")
    @ResponseBody
    public ResponseEntity list(){
        return ResponseEntity.status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .body(this.Service.list().stream().limit(100).toList());
    }

}
