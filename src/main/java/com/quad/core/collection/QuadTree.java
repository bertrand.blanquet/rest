package com.quad.core.collection;


import com.quad.core.BoundingBox;
import com.quad.core.Drawable;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class QuadTree<T extends Drawable> {
    private QuadNode<T> root;
    private BoundingBox box;
    private int threshold = 5;
    private int count = 0;

    public QuadTree(BoundingBox canvas, int threshold){
        this.box = canvas;
        this.threshold = threshold;
    }
    public void add(T element) {
        if(this.root == null){
            this.root = new QuadNode(this.box, this.threshold);
        }
        this.root.add(element);
        this.count++;
    }

    public int size(){
        return this.count;
    }

    public Stream<T> contains(BoundingBox rectangle) {
        var result = new ArrayList<T>();
        this.root.contains(rectangle,result);
        return result.stream().distinct();
    }

    public void setThreshold(int i) {
        this.threshold = i;
    }

    public int maxDepth() {
        var leaves = this.leaves(this.root);
        var depths = leaves.stream().map(n->n.getDepth()).toList();
        var theDepth = depths.stream().max(Integer::compare).get();
        return theDepth;
    }

    private ArrayList<QuadNode<T>> leaves(QuadNode<T> current){
        if(current.isLeaf()){
            return new ArrayList<QuadNode<T>>(List.of(current));
        }else{
            var result = new ArrayList<QuadNode<T>>();
            current.children().forEach(c->{
                result.addAll(this.leaves(c));
            });
            return result;
        }
    }

    public void remove(BoundingBox boundingBox) {
        this.count -= this.root.remove(boundingBox);
    }

    public void paint(Graphics2D g) {
        if(this.root != null){
            this.root.paint(g);
        }
    }
}
