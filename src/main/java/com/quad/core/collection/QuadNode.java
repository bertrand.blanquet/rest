package com.quad.core.collection;

import com.quad.core.BoundingBox;
import com.quad.core.Drawable;

import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;

public class QuadNode<T extends Drawable> {
    private int threshold = 0;
    private BoundingBox box;
    private ArrayList<T> items;

    private QuadNode<T> parent;

    private QuadNode<T> topLeft;
    private QuadNode<T> topRight;
    private QuadNode<T> bottomLeft;
    private QuadNode<T> bottomRight;
    private QuadNode<T>[] children;

    QuadNode(BoundingBox box, int threshold){
        this.threshold = threshold;
        this.items = new ArrayList<T>();
        this.box = box;
    }

    public boolean isRoot(){
        return this.parent == null;
    }

    public boolean isLeaf(){
        return this.topLeft == null
                && this.topRight == null
                && this.bottomLeft == null
                && this.bottomRight == null;
    }


    public void add(T element) {
        if(this.isLeaf()){
            if(this.items.size() < this.threshold){
                this.items.add(element);
            }else{
                this.split();
                this.items.forEach(i->{
                    this.injectToChildren(i);
                });
                this.items.clear();
            }
        }else{
            injectToChildren(element);
        }
    }

    private void injectToChildren(T item) {
        for (var i = 0; i < this.children.length; i++) {
            var child = this.children[i];
            if(child.getBox().intersect(item.getBox())){
                child.add(item);
            }
        }
    }

    private void split() {
        var halfWidth = this.box.getWidth()/2;
        var halfHeight = this.box.getHeight()/2;

        this.topLeft = new QuadNode(new BoundingBox(this.box.getX(),this.box.getY(), halfWidth,halfHeight),this.threshold);
        this.topLeft.setParent(this);

        this.topRight = new QuadNode(new BoundingBox(this.box.getX()+halfWidth,this.box.getY(), halfWidth,halfHeight),this.threshold);
        this.topRight.setParent(this);

        this.bottomLeft = new QuadNode(new BoundingBox(this.box.getX(),this.box.getY()+halfHeight, halfWidth,halfHeight),this.threshold);
        this.bottomLeft.setParent(this);

        this.bottomRight = new QuadNode(new BoundingBox(this.box.getX()+halfWidth,this.box.getY()+halfHeight, halfWidth,halfHeight),this.threshold);
        this.bottomRight.setParent(this);

        this.children = new QuadNode[]{this.topLeft, this.topRight, this.bottomLeft, this.bottomRight};

    }

    public BoundingBox getBox(){
        return this.box;
    }

    public void setParent(QuadNode parent){
        this.parent = parent;
    }

    public QuadNode getParent(){
        return this.parent;
    }

    public void contains(BoundingBox rectangle, ArrayList<T> result) {
        if(this.isLeaf()){
            result.addAll(this.items.stream().filter(i->rectangle.contains(i.getBox())).toList());
        }else{
            for (var i = 0; i < this.children.length; i++) {
                var child = this.children[i];
                if(child.getBox().intersect(rectangle)){
                    child.contains(rectangle,result);
                }
            }
        }
    }

    public int getDepth(){
        var i = 0;
        var parent = this.parent;
        while(parent != null){
            parent = parent.getParent();
            i++;
        }
        return i;
    }

    public ArrayList<QuadNode> children() {
        return new ArrayList<QuadNode>(Arrays.stream(this.children).toList());
    }

    public int remove(BoundingBox boundingBox) {
        if(this.isLeaf()){
            var before = this.items.size();
            this.items = new ArrayList<>(this.items.stream().filter(item->boundingBox.intersect(item.getBox())).toList());
            if(!this.isRoot()){
                this.parent.itemsRemoved();
            }
            var after = this.items.size();
            return before-after;
        }else{
            var deleted = 0;
            for (var i = 0; i < this.children.length; i++) {
                var child = this.children[i];
                if (child.getBox().intersect(boundingBox)) {
                    deleted += child.remove(boundingBox);
                }
            }
            return deleted;
        }
    }

    private void itemsRemoved() {
        if(this.size() < this.threshold){
            this.items = this.getAll();
            this.killChildren();
            if(!this.isRoot()){
                this.parent.itemsRemoved();
            }
        }
    }

    private void killChildren() {
        this.bottomLeft = null;
        this.bottomRight = null;
        this.topRight = null;
        this.topLeft = null;
    }

    private ArrayList<T> getAll() {
        if(this.isLeaf()){
            return this.items;
        }else{
            var items = new ArrayList<T>();
            for (var i = 0; i < this.children.length; i++) {
                items.addAll(this.children[i].getAll());
            }
            return items;
        }
    }

    private int size() {
        var size = 0;
        if(this.isLeaf()){
            size = this.items.size();
        }else{
            for (var i = 0; i < this.children.length; i++) {
                size  += this.children[i].size();
            }
        }
        return size;
    }

    public void paint(Graphics2D g) {
        g.setPaint(Color.white);
        var nodeStroke =
                g.getStroke()
                        .createStrokedShape(
                                new Rectangle(this.box.getX(),this.box.getY(),this.box.getWidth(),this.box.getHeight()));
        g.draw(nodeStroke);
        if(!this.isLeaf()){
            for (var i = 0; i< this.children.length; i++){
                this.children[i].paint(g);
            }
        }else{
            this.items.forEach(item->{
                item.paint(g);
            });
        }
    }
}
