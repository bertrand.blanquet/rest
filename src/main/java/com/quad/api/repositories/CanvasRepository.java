package com.quad.api.repositories;
import com.quad.api.model.Canvas;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CanvasRepository extends JpaRepository<Canvas, Long>, JpaSpecificationExecutor<Canvas> {
    public default Optional<Canvas> getCanvasFromName(String name){
        var spec = new CustomSpecification<Canvas>(
                new SearchCriteria<Canvas>("Label", ":", name));
        return this.findOne(spec);
    }
}