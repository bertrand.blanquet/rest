package com.quad.api.services;

import com.quad.api.model.Canvas;
import com.quad.api.model.Item;
import com.quad.api.model.ResearchInfo;
import com.quad.api.repositories.CanvasRepository;
import com.quad.api.repositories.CanvasItemRepository;
import com.quad.core.BoundingBox;
import com.quad.core.CanvasItem;
import com.quad.core.collection.QuadTree;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;


@Service
public class CanvasService implements CanvasActions {
    @Autowired
    public CanvasRepository Repository;
    @Autowired
    public CanvasItemRepository ItemRepository;

    public void add(String name, int width, int height, int threshold){
        this.Repository.save(new Canvas(name,width,height,threshold));
    }

    public List<Canvas> list(){
        return this.Repository.findAll();
    }

    public List<ResearchInfo> search(String name, BoundingBox box){
        var result = new ArrayList<ResearchInfo>();
        var maybeCanvas = this.Repository.getCanvasFromName(name);
        if(maybeCanvas.isPresent()){
            var canvas = maybeCanvas.get();
            var items = this.ItemRepository.getItemsFromCanvasId(canvas.getId());
            var tree = this.GetTree(canvas,items);
            result.add(GetListResult(items.size(), box, items));
            result.add(GetTreeResult(items.size(), box, tree));
            return result;
        }else{
            return null;
        }
    }

    @Override
    public boolean isFull() {
        return 50 < this.Repository.count();
    }

    @Override
    public boolean exist(String name) {
        return this.Repository.getCanvasFromName(name).isPresent();
    }

    private ResearchInfo GetTreeResult(int count, BoundingBox box, QuadTree<CanvasItem> tree) {
        var now = System.nanoTime();
        var treeResult = tree.contains(box);
        var duration = System.nanoTime() - now;
        return new ResearchInfo(MessageFormat.format("Tree [{0}]",count),treeResult.map(i->i.Label).toList(), MessageFormat.format("{0} ns",Long.toString(duration)));
    }

    private ResearchInfo GetListResult(int count, BoundingBox box, List<Item> items) {
        var now = System.nanoTime();
        var listResult=items.stream().filter(i->box.contains(i.getBox())).toList();
        var duration = System.nanoTime() - now;
        return new ResearchInfo(MessageFormat.format("List [{0}]",count),listResult.stream().map(i->i.getLabel()).toList(), MessageFormat.format("{0} ns",Long.toString(duration)));
    }

    public byte[] getImg(String name) throws IOException {
        var maybeCanvas = this.Repository.getCanvasFromName(name);
        if(maybeCanvas.isPresent()){
            var canvas = maybeCanvas.get();
            var items = this.ItemRepository.getItemsFromCanvasId(canvas.getId());

            QuadTree tree = GetTree(canvas, items);

            var image = new BufferedImage(canvas.getWidth(),canvas.getHeight(),BufferedImage.TYPE_INT_RGB);
            var graph = image.createGraphics();
            var os = new ByteArrayOutputStream();
            tree.paint(graph);
            ImageIO.write(image, "jpg", os);
            InputStream is = new ByteArrayInputStream(os.toByteArray());
            return IOUtils.toByteArray(is);
        }else{
            return null;
        }
    }

    private QuadTree<CanvasItem> GetTree(Canvas canvas, List<Item> items) {
        var tree = new QuadTree<CanvasItem>(new BoundingBox(0,0, canvas.getWidth(), canvas.getHeight()),10);
        items.forEach(i->{
            var item = new CanvasItem(i.getX(),i.getY(),i.getWidth(),i.getHeight());
            item.FillColor = i.GetFillColor();
            item.StrokeColor = i.GetStokeColor();
            item.Label = i.getLabel();
            tree.add(item);
        });
        return tree;
    }
}
