package com.quad.core;

public interface Boxable extends Labeled {
    BoundingBox getBox();
}
