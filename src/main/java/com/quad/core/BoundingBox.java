package com.quad.core;

public class BoundingBox {
    private final int x;
    private final int y;
    private final int width;
    private final int height;

    public BoundingBox(int x, int y, int width, int height) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }

    public int bottom(){
        return this.y +this.height;
    }

    public int right() {
        return this.x + this.width;
    }

    public boolean contains(BoundingBox r) {
        return (this.x <= r.x
                && r.right() <= this.right()
                && this.y <= r.y
                && r.bottom() <= this.bottom());
    }

    public boolean intersect(BoundingBox rectangle) {
        return !(this.x > rectangle.right() ||
                this.right() < rectangle.x ||
                this.y > rectangle.bottom() ||
                this.bottom() < rectangle.y);
    }
    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }
}
