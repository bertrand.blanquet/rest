package com.quad;

import com.quad.api.RestApplication;
import com.quad.core.BoundingBox;
import com.quad.core.CanvasItem;
import com.quad.core.collection.QuadTree;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.util.Assert.isTrue;

@SpringBootTest(classes = RestApplication.class)
class QuadTests {
    private QuadTree _tree;

    private ArrayList<CanvasItem> GetSix(){
        return new ArrayList<CanvasItem>(List.of(
                new CanvasItem(1,1,10,10),
                new CanvasItem(10,1,10,10),
                new CanvasItem(1,10,10,10),
                new CanvasItem(30,30,10,10),
                new CanvasItem(30,30,10,10),
                new CanvasItem(30,30,10,10)));
    }

    private ArrayList<CanvasItem> GetTwo(){
        return new ArrayList<CanvasItem>(List.of(
                new CanvasItem(1,1,10,10),
                new CanvasItem(20,1,10,10)));
    }

    @BeforeEach
    void Init(){
        this._tree = new QuadTree(new BoundingBox(0,0,1000,1000),5);
    }

    @Test
    void AddElementInNode() {
        var element = new CanvasItem(0,0,10,10);
        this._tree.add(element);
        isTrue(this._tree.size()==1,"should be inserted.");
    }

    @Test
    void RetrieveElementInNode(){
        var element = new CanvasItem(0,0,10,10);
        this._tree.add(element);
        var result = this._tree.contains(new BoundingBox(0,0,15,15)).toList().size();
        Assert.state(result==1,"should retrieve one element but retrieved " + result);
    }

    @Test
    void GetTwoLayoutWhenAddingMoreThanThresholdExceeded(){
        this._tree.setThreshold(5);
        for (var i = 0; i < 8; i++){
            this._tree.add(new CanvasItem(i,i,i,i));
        }
        var depth = this._tree.maxDepth();
        isTrue(depth==3,String.format("max depth is not equal to 1 but %s",depth));
    }

    @Test
    void GetLessItemsAfterRemovingItems(){
        this.GetTwo().forEach(n->{
            this._tree.add(n);
        });
        this._tree.remove(this.GetTwo().get(0).getBox());
        isTrue(this._tree.size()==1,"should only have one element");
    }
}
