package com.quad.api.repositories;

import com.quad.api.model.Item;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface CanvasItemRepository extends JpaRepository<Item, Long>, JpaSpecificationExecutor<Item> {

    public default List<Item> getItemsFromCanvasId(int canvasId){
        var spec = new CustomSpecification<Item>(
                new SearchCriteria<Item>("CanvasId", ":", canvasId));
        return this.findAll(spec);
    }
}
